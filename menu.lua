-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------
local store = require( "store" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"
local audio = require("audio")
--------------------------------------------

-- forward declarations and other locals
local playBtn
local soundToggle
local musicToggle
local group

local titleField
local nameLabel
local nameBtn
local scoreGroup
local coinText
local expText

local touchHighScoresBtn = function( event )
	if event.phase == "release" then
		local webListener = function( event )
			local shouldLoad = true
 
			local url = event.url
			if 1 == string.find( url, "corona:close" ) then
				-- Close the web popup
				shouldLoad = false
			end
 
			return shouldLoad
		end
 
		-- show web popup
		local showRanksUrl = "http://www.silentlogicstudios.com/match_app/facebookleaderboard/loadfriendranks/?bestscore=" .. scList:GetPoints(1)
		native.showWebPopup( 10, display.contentHeight-300, 300, 300, showRanksUrl, { urlRequest=webListener } )
	end
end

local touchLocalScoresBtn = function( event )
	if event.phase == "release" then
		native.cancelWebPopup()
	end
end

local function onNameBtnRelease()
	
	local textFont = native.newFont( native.systemFont )
	if titleField then
	   local len = string.len(titleField.text)
	   if len > 0 and len <= MAX_NAME_LENGTH then
	       RECORD_DATA[RECORD_NAME] = titleField.text
    	   nameBtn:setLabel("Player Name: " .. RECORD_DATA[RECORD_NAME])
    	   saveData()
    	   titleField:removeSelf() 
    	   titleField = nil
	   
    	   nameLabel:removeSelf()
    	   nameLabel = nil
	   else
	       nameLabel.text = "Name must be shorter than " .. MAX_NAME_LENGTH .. " characters."
	       nameLabel:setReferencePoint(display.TopLeftReferencePoint) 
	       nameLabel.x = 10
	   end
	else
	    titleField = native.newTextField( 10, 70, display.contentWidth-(10*2), 28 )
	    titleField.text = RECORD_DATA[RECORD_NAME]
        titleField.font = textFont
        titleField.size = 14
        group:insert(titleField)
        
        nameLabel = display.newText( "", 10, 40, "Arial", 15 )
        nameLabel.text = "Enter Name" 
        nameLabel:setReferencePoint(display.TopLeftReferencePoint) 
        nameLabel.x = 10
        nameLabel:setTextColor(255, 255, 255, 255 )
        group:insert(nameLabel)
        
    end
    
	return true	-- indicates successful touch
end


-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "level1", "fade", 500 )
	
	return true	-- indicates successful touch
end

local function onScoreBtnRelease()
	
	storyboard.gotoScene( "leaderboard", "fade", 500 )
	
	return true	-- indicates successful touch
end

local function onHelpBtnRelease()
	
	storyboard.gotoScene( "help", "fade", 500 )
	
	return true	-- indicates successful touch
end

local function createMusicToggle(group)
    if RECORD_DATA[RECORD_MUSIC] == 1 then
        musicToggle = display.newImage ("Images/soundon.png")
        media.stopSound()
    
        if backgroundMusicChannel == -1 then
            -- if backgroundMusic then
            --                 audio.dispose( backgroundMusic )
            --             end
            
            if backgroundMusic == nil then
            --    backgroundMusic = audio.loadStream("lost_mp3.mp3")
            end
            backgroundMusicChannel = audio.play( backgroundMusic, { channel=1, loops=-1 }  )
        end
    else
        musicToggle = display.newImage ("Images/soundoff.png")
        media.stopSound()
        
        audio.stop( backgroundMusicChannel )
        backgroundMusicChannel = -1
        
    end
    
    musicToggle.x = 80
    musicToggle.y = display.contentHeight-30
    
    group:insert(musicToggle)
    
    local soundText = display.newText( "", musicToggle.x-40, musicToggle.y-32*0.5, "MarkerFelt-Thin", 18 )
    soundText:setReferencePoint(display.CenterReferencePoint) 
    soundText.text = "Music" 
    soundText:setTextColor(255, 255, 255, 255 )
    group:insert(soundText)
    
    local function toggleMusic (event)
        if event.phase == "ended" then
            musicToggle:removeSelf()
            if RECORD_DATA[RECORD_MUSIC] == 1 then
                RECORD_DATA[RECORD_MUSIC] = 0
            else
                RECORD_DATA[RECORD_MUSIC] = 1
            end
            createMusicToggle(group)
            print ("TOGGLE " .. RECORD_DATA[RECORD_MUSIC])
            musicToggle.x = 80
            musicToggle.y = display.contentHeight-30
            saveData()
        end
    end

    musicToggle:addEventListener ("touch", toggleMusic)
    
end


local function createSoundToggle(group)
    if RECORD_DATA[RECORD_SOUND] == 1 then
        soundToggle = display.newImage ("Images/soundon.png")
      --  media.stopSound()
    else
        soundToggle = display.newImage ("Images/soundoff.png")
     --   media.stopSound()
    end
    
    soundToggle.x = display.contentWidth-32*0.5 - 10
    soundToggle.y = display.contentHeight - 30
    
    group:insert(soundToggle)
    
    local soundText = display.newText( "", soundToggle.x-40, soundToggle.y-32*0.5, "MarkerFelt-Thin", 18 )
    soundText:setReferencePoint(display.CenterReferencePoint) 
    soundText.text = "Sound" 
    soundText:setTextColor(255, 255, 255, 255 )
    group:insert(soundText)
    local function toggleSound (event)
        if event.phase == "ended" then
            soundToggle:removeSelf()
            if RECORD_DATA[RECORD_SOUND] == 1 then
                RECORD_DATA[RECORD_SOUND] = 0
            else
                RECORD_DATA[RECORD_SOUND] = 1
                media.playEventSound("Sounds/whoosh.wav")
            end
            createSoundToggle(group)
            print ("TOGGLE " .. RECORD_DATA[RECORD_SOUND])
            soundToggle.x = display.contentWidth-32*0.5 - 10
            soundToggle.y = display.contentHeight - 30
            saveData()
        end
    end

    soundToggle:addEventListener ("touch", toggleSound)
    
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	group = self.view

	-- display a background image
	local background = display.newImageRect( IMAGE_PATH .. "title.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	
	local scorebg_height = 300
	local scoreBG = display.newImageRect( IMAGE_PATH .. "score_bg.png", 320, scorebg_height )
	scoreBG:setReferencePoint( display.TopLeftReferencePoint )
	scoreBG.x = 0
	scoreBG.y = display.contentHeight-scorebg_height
--	create a widget button (which will loads level1.lua on release)
	   playBtn = widget.newButton{
	           label="",
	           labelColor = { default={255}, over={128} },
	           default=IMAGE_PATH .. "btn-play-normal.png",
	           over=IMAGE_PATH .. "btn-play-selected.png",
	           width=154, height=40,
	           onRelease = onPlayBtnRelease    -- event listener function
	       }
           playBtn:setReferencePoint( display.CenterReferencePoint )
           playBtn.x = 250
           playBtn.y = 50
	
       scoreBtn = widget.newButton{
           label="",
           labelColor = { default={255}, over={128} },
           default=IMAGE_PATH .. "btn-scores-normal.png",
           over=IMAGE_PATH .. "btn-scores-selected.png",
           width=154, height=40,
           onRelease = touchHighScoresBtn   -- event listener function
       }
       scoreBtn:setReferencePoint( display.CenterReferencePoint )
       scoreBtn.x = 280
       scoreBtn.y = 150
       
       localBtn = widget.newButton{
              label="",
              labelColor = { default={255}, over={128} },
              default=IMAGE_PATH .. "btn-scores-normal.png",
              over=IMAGE_PATH .. "btn-scores-selected.png",
              width=154, height=40,
              onRelease = touchLocalScoresBtn   -- event listener function
          }
          localBtn:setReferencePoint( display.CenterReferencePoint )
          localBtn.x = 50
          localBtn.y = 150
	
	helpBtn = widget.newButton{
		label="",
		labelColor = { default={255}, over={128} },
		default=IMAGE_PATH .. "btn-help-normal.png",
		over=IMAGE_PATH .. "btn-help-selected.png",
		width=154, height=40,
		onRelease = onHelpBtnRelease	-- event listener function
	}
	helpBtn:setReferencePoint( display.CenterReferencePoint )
	helpBtn.x = 250
	helpBtn.y = 100


    coinText = display.newText( "", display.contentWidth*0.5, 2, GAME_FONT, 16 )
	coinText:setReferencePoint(display.TopRightReferencePoint) 
	coinText.text = RECORD_DATA[RECORD_GOLD] .. " coins"
	coinText:setTextColor(0,0,0,255)
	coinText.x = 100
	coinText.y = 20

	expText = display.newText( "", display.contentWidth*0.5, 2, GAME_FONT, 16 )
	expText:setReferencePoint(display.TopRightReferencePoint) 
	expText.text = RECORD_DATA[RECORD_EXP] .. " xp"
	expText:setTextColor(0,0,0,255)
	expText.x = 100
	expText.y = 40
	
    
	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( scoreBG )
	group:insert( playBtn )	-- you must insert .view property for widgets
--	group:insert( scoreBtn )	
	group:insert( helpBtn )
	group:insert(localBtn)
	group:insert(scoreBtn)
	group:insert(coinText)
	group:insert(expText)
	createSoundToggle(group)
    --createMusicToggle(group)
    
    nameBtn = widget.newButton{
		label="Player Name: " .. RECORD_DATA[RECORD_NAME],
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		default== "",
		over="",
		width=158, height=30,
		onRelease = onNameBtnRelease	-- event listener function
	}
	nameBtn:setReferencePoint( display.CenterReferencePoint )
	nameBtn.x = 90
	nameBtn.y = 480-30
	group:insert( nameBtn )
	
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view

   coinText.text = RECORD_DATA[RECORD_GOLD] .. " coins"
   expText.text = RECORD_DATA[RECORD_EXP] .. " xp"
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)

  --   storyboard.removeScene( "level1" )
     
     local group = self.view
 	scoreGroup = display.newGroup()
 	group:insert(scoreGroup)
 	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
 	for i=1,numberOfEntries do
 	    print ("SCORE NAME: " .. scList:GetName(i))
 	    print ("SCORE Points: " .. scList:GetPoints(i))

 	    local name = display.newText( "", 0, 0, "Arial-BoldMT", 16 )
         name.text = i .. ". " .. scList:GetName(i)
         name:setReferencePoint(display.TopLeftReferencePoint) 

         name.x = 30
         name.y = 200+30*(i-1)
         name:setTextColor(255, 255, 255, 255 )

         local scoreNum = display.newText( "", 0,0 , "Arial-BoldMT", 16 )
         scoreNum.text = scList:GetPoints(i)
         scoreNum:setReferencePoint(display.TopRightReferencePoint) 
         scoreNum.x = 300
         scoreNum.y = 200+30*(i-1)
         scoreNum:setTextColor(255, 255, 255, 255 )

 	    scoreGroup:insert(name)
 	    scoreGroup:insert(scoreNum)
 	end
 	
 	if RECORD_DATA[RECORD_UNLOCKED_BOMB] == 0 and RECORD_DATA[RECORD_EXP] >= POWER1_EXP then
 	    RECORD_DATA[RECORD_UNLOCKED_BOMB]  = 1
 	    saveData()
 	    print "UNLOCK BOMB"
 	end
 	
 	if RECORD_DATA[RECORD_UNLOCKED_FLARE] == 0 and RECORD_DATA[RECORD_EXP] >= POWER2_EXP then
 	    RECORD_DATA[RECORD_UNLOCKED_FLARE]  = 1
 	    saveData()
 	     print "UNLOCK FLARE"
 	end
 	
 	if RECORD_DATA[RECORD_UNLOCKED_STAR] == 0 and RECORD_DATA[RECORD_EXP] >= POWER3_EXP then
 	    RECORD_DATA[RECORD_UNLOCKED_STAR]  = 1
 	    saveData()
 	     print "UNLOCK STAR"
 	end
 	
 	if RECORD_DATA[RECORD_UNLOCKED_PLOW] == 0 and RECORD_DATA[RECORD_EXP] >= POWER4_EXP then
 	    RECORD_DATA[RECORD_UNLOCKED_PLOW]  = 1
 	    saveData()
 	     print "UNLOCK PLOW"
 	end

end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	scoreGroup:removeSelf()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene