-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"
local group
--------------------------------------------
local titleField
local nameLabel
local nameBtn
local scoreGroup
-- 'onRelease' event listener for playBtn
local function onBackBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "menu", "fade", 500 )
	
	return true	-- indicates successful touch
end

local function onNameBtnRelease()
	
	local textFont = native.newFont( native.systemFont )
	if titleField then
	   local len = string.len(titleField.text)
	   if len > 0 and len <= MAX_NAME_LENGTH then
	       RECORD_DATA[RECORD_NAME] = titleField.text
    	   nameBtn:setLabel("Player Name: " .. RECORD_DATA[RECORD_NAME])
    	   saveData()
    	   titleField:removeSelf() 
    	   titleField = nil
	   
    	   nameLabel:removeSelf()
    	   nameLabel = nil
	   else
	       nameLabel.text = "Name must be shorter than " .. MAX_NAME_LENGTH .. " characters."
	       nameLabel:setReferencePoint(display.TopLeftReferencePoint) 
	       nameLabel.x = 10
	   end
	else
	    titleField = native.newTextField( 10, 70, display.contentWidth-(10*2), 28 )
	    titleField.text = RECORD_DATA[RECORD_NAME]
        titleField.font = textFont
        titleField.size = 14
        group:insert(titleField)
        
        nameLabel = display.newText( "", 10, 40, "Arial", 15 )
        nameLabel.text = "Enter Name" 
        nameLabel:setReferencePoint(display.TopLeftReferencePoint) 
        nameLabel.x = 10
        nameLabel:setTextColor(255, 255, 255, 255 )
        group:insert(nameLabel)
        
    end
    
	return true	-- indicates successful touch
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	group = self.view

	-- display a background image
	local background = display.newImageRect( IMAGE_PATH .. "starfield.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	
	-- create a widget button (which will loads level1.lua on release)
	backBtn = widget.newButton{
		label="Menu",
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		default== "",
		over="",
		width=68, height=30,
		onRelease = onBackBtnRelease	-- event listener function
	}
	backBtn:setReferencePoint( display.CenterReferencePoint )
	backBtn.x = display.contentWidth*0.5
	backBtn.y = display.contentHeight-25
	

	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( backBtn )	-- you must insert .view property for widgets
	
	nameBtn = widget.newButton{
		label="Player Name: " .. RECORD_DATA[RECORD_NAME],
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		default== "",
		over="",
		width=158, height=30,
		onRelease = onNameBtnRelease	-- event listener function
	}
	nameBtn:setReferencePoint( display.CenterReferencePoint )
	nameBtn.x = display.contentWidth*0.5
	nameBtn.y = 20
	group:insert( nameBtn )
	
	
	
	
	
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	scoreGroup = display.newGroup()
	group:insert(scoreGroup)
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	for i=1,numberOfEntries do
	    print ("SCORE NAME: " .. scList:GetName(i))
	    print ("SCORE Points: " .. scList:GetPoints(i))
	    
	    local name = display.newText( "", 0, 0, "Arial-BoldMT", 16 )
        name.text = i .. ". " .. scList:GetName(i)
        name:setReferencePoint(display.TopLeftReferencePoint) 
        
        name.x = 30
        name.y = 100+30*(i-1)
        name:setTextColor(255, 255, 255, 255 )
        
        local scoreNum = display.newText( "", 0,0 , "Arial-BoldMT", 16 )
        scoreNum.text = scList:GetPoints(i)
        scoreNum:setReferencePoint(display.TopRightReferencePoint) 
        scoreNum.x = 300
        scoreNum.y = 100+30*(i-1)
        scoreNum:setTextColor(255, 255, 255, 255 )
        
	    scoreGroup:insert(name)
	    scoreGroup:insert(scoreNum)
	end
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	scoreGroup:removeSelf()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene