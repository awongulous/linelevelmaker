-----------------------------------------------------------------------------------------
--
-- selection.lua
-- show powerups
-- enable ones that are unlocked
-- set the ones the user selects (costs gold)
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require "widget"
local ads = require "ads"
--------------------------------------------

-- forward declarations and other locals
local playBtn
local group
local coinText
local expText

local power1
local power2
local power3
local power4
local IAP_FULL = "full"
local IAP_COIN_1000 = "coin1000"

local function showThanks()
    local function onAlert( event )
    end

    local alert = native.showAlert( "", "Thanks for purchasing the game!", { "OK" }, onAlert )    
end

local function removeFullPurchase()
    -- remove full purchase button
end


local function transactionCallback( event )
    local transaction = event.transaction

    if transaction.state == "purchased" then
    
        -- Transaction was successful; unlock/download content now
        showThanks()
        
        if transaction.productIdentifier == IAP_FULL then
            RECORD_DATA[RECORD_PURCHASED] = 1
            RECORD_DATA[RECORD_UNLOCKED_BOMB]  = 1
            RECORD_DATA[RECORD_UNLOCKED_FLARE]  = 1
            RECORD_DATA[RECORD_UNLOCKED_STAR]  = 1
            RECORD_DATA[RECORD_UNLOCKED_PLOW]  = 1
            removeFullPurchase()
        elseif transaction.productIdentifier == IAP_COIN_1000 then
            RECORD_DATA[RECORD_GOLD]  = RECORD_DATA[RECORD_GOLD] + 1000
        end
        saveData()

    elseif  transaction.state == "restored" then
    
       -- You'll never reach this transaction state on Android.
       showThanks()
       if transaction.productIdentifier == IAP_FULL then
           RECORD_DATA[RECORD_PURCHASED] = 1
           RECORD_DATA[RECORD_UNLOCKED_BOMB]  = 1
           RECORD_DATA[RECORD_UNLOCKED_FLARE]  = 1
           RECORD_DATA[RECORD_UNLOCKED_STAR]  = 1
           RECORD_DATA[RECORD_UNLOCKED_PLOW]  = 1
           elseif transaction.productIdentifier == IAP_COIN_1000 then
               RECORD_DATA[RECORD_GOLD]  = RECORD_DATA[RECORD_GOLD] + 1000

           end
       saveData()

    elseif  transaction.state == "refunded" then
    
        -- Android-only; user refunded their purchase
        local productId = transaction.productIdentifier

        -- Restrict/remove content associated with above productId now
        if transaction.productIdentifier == IAP_FULL then
            RECORD_DATA[RECORD_PURCHASED] = 0
        end
        saveData()
        setToLiteVersion()
    
    elseif transaction.state == "cancelled" then
    
        -- Transaction was cancelled; tell you app to react accordingly here


    elseif transaction.state == "failed" then        
    
        -- Transaction failed; tell you app to react accordingly here
    
    end

    -- The following must be called after transaction is complete.
    -- If your In-app product needs to download, do not call the following
    -- function until AFTER the download is complete:

    store.finishTransaction( transaction )
end

local function onPurchaseBtnRelease()
    -- do purchase
    store.purchase( {"com.silentlogicstudios.gravulous.full"})
	return true	-- indicates successful touch
end


-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "level1", "fade", 500 )
	
	return true	-- indicates successful touch
end

local function onPower1BtnRelease()
	
	if HAS_BOMB then
	    HAS_BOMB = false
	else
	    HAS_BOMB = true
	end
	
	-- recreate button
	self:createPowerButtons()
	
	return true	-- indicates successful touch
end

local function onPower2BtnRelease()
	if HAS_FLARE then
	    HAS_FLARE = false
	else
	    HAS_FLARE = true
	end
	
	-- recreate button
	self:createPowerButtons()
	return true	-- indicates successful touch
end

local function onPower3BtnRelease()
	if HAS_STAR then
	    HAS_STAR = false
	else
	    HAS_STAR = true
	end
	
	-- recreate button
	self:createPowerButtons()
	return true	-- indicates successful touch
end

local function onPower4BtnRelease()
	if HAS_PLOW then
	    HAS_PLOW = false
	else
	    HAS_PLOW = true
	end
	
	-- recreate button
	self:createPowerButtons()
	return true	-- indicates successful touch
end

local function onBackBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "menu", "fade", 500 )
	
	return true	-- indicates successful touch
end


local function setToLiteVersion()
    
    if liteText == nil then
    liteText = display.newText( "", 160, 450, "Arial", 16 )
    liteText:setReferencePoint(display.CenterReferencePoint) 
    liteText.text = "Lite Version"
    liteText:setTextColor(255, 255, 255, 255 )
    group:insert(liteText)
    end
    
    
    if purchaseBtn == nil then
    purchaseBtn = widget.newButton{
		label="Buy Full Version",
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		width=150, height=30,
		onRelease = onPurchaseBtnRelease	-- event listener function
	}
	purchaseBtn:setReferencePoint( display.CenterReferencePoint )
	purchaseBtn.x = display.contentWidth*0.5
	purchaseBtn.y = display.contentHeight*0.5 + 40*3
	
	group:insert( purchaseBtn )
	 
    end
end

function scene:createPowerButtons()
    
    if power1 then
        power1:destroy()
    end
    if power2 then
        power2:destroy()
    end
    if power3 then
        power3:destroy()
    end
    if power4 then
        power4:destroy()
    end
    
    if RECORD_DATA[RECORD_UNLOCKED_BOMB] == 0 then
        power1 = display.newImageRect("Images/btn-play-selected.png", 154,40) 
        power1.x= 50
        power1.y= 150
        
    else
        power1 = widget.newButton{
            label="power1",
            labelColor = { default={255}, over={128} },
            default=IMAGE_PATH .. "btn-play-normal.png",
            over=IMAGE_PATH .. "btn-play-selected.png",
            width=154, height=40,
            onRelease = onPower1BtnRelease    -- event listener function
        }
        power1:setReferencePoint( display.CenterReferencePoint )
        power1.x = 50
        power1.y = 150
    end

    if RECORD_DATA[RECORD_UNLOCKED_FLARE] == 0 then
        power2 = display.newImageRect("Images/btn-play-selected.png", 154,40) 
        power2.x = 100
        power2.y = 150
    else
        power2 = widget.newButton{
            label="power2",
            labelColor = { default={255}, over={128} },
            default=IMAGE_PATH .. "btn-play-normal.png",
            over=IMAGE_PATH .. "btn-play-selected.png",
            width=154, height=40,
            onRelease = onPower2BtnRelease    -- event listener function
        }
        power2:setReferencePoint( display.CenterReferencePoint )
        power2.x = 100
        power2.y = 150
    end
                  
    if RECORD_DATA[RECORD_UNLOCKED_STAR] == 0 then
        power3 = display.newImageRect("Images/btn-play-selected.png", 154,40) 
        power3.x = 150
        power3.y = 150
    else       
        power3 = widget.newButton{
            label="power3",
            labelColor = { default={255}, over={128} },
            default=IMAGE_PATH .. "btn-play-normal.png",
            over=IMAGE_PATH .. "btn-play-selected.png",
            width=154, height=40,
            onRelease = onPower3BtnRelease    -- event listener function
        }
        power3:setReferencePoint( display.CenterReferencePoint )
        power3.x = 150
        power3.y = 150
    end
    
    if RECORD_DATA[RECORD_UNLOCKED_PLOW] == 0 then
        power4 = display.newImageRect("Images/btn-play-selected.png", 154,40) 
        power4.x = 200
        power4.y = 150
    else
        power4 = widget.newButton{
            label="power4",
            labelColor = { default={255}, over={128} },
            default=IMAGE_PATH .. "btn-play-normal.png",
            over=IMAGE_PATH .. "btn-play-selected.png",
            width=154, height=40,
            onRelease = onPower4BtnRelease    -- event listener function
        }
        power4:setReferencePoint( display.CenterReferencePoint )
        power4.x = 200
        power4.y = 150
    end 
    
    group:insert(power1)
    group:insert(power2)
    group:insert(power3)
    group:insert(power4)
    
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	group = self.view

	-- display a background image
	local background = display.newImageRect( IMAGE_PATH .. "title.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	
	
	   playBtn = widget.newButton{
	           label="",
	           labelColor = { default={255}, over={128} },
	           default=IMAGE_PATH .. "btn-play-normal.png",
	           over=IMAGE_PATH .. "btn-play-selected.png",
	           width=154, height=40,
	           onRelease = onPlayBtnRelease    -- event listener function
	       }
           playBtn:setReferencePoint( display.CenterReferencePoint )
           playBtn.x = 250
           playBtn.y = 50
	
      
	backBtn = widget.newButton{
		label="",
		labelColor = { default={255}, over={128} },
		default=IMAGE_PATH .. "btn-help-normal.png",
		over=IMAGE_PATH .. "btn-help-selected.png",
		width=154, height=40,
		onRelease = onBackBtnRelease	-- event listener function
	}
	backBtn:setReferencePoint( display.CenterReferencePoint )
	backBtn.x = 250
	backBtn.y = 100

	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( playBtn )	-- you must insert .view property for widgets
	group:insert( backBtn )

    -- display power ups, graphic depends on enabled, disabled or locked
    self:createPowerButtons()
           
    
    -- display exp, coins
    coinText = display.newText( "", display.contentWidth*0.5, 2, GAME_FONT, 16 )
	coinText:setReferencePoint(display.TopRightReferencePoint) 
	coinText.text = RECORD_DATA[RECORD_GOLD] .. " coins"
	coinText:setTextColor(0,0,0,255)
	coinText.x = 100
	coinText.y = 20

	expText = display.newText( "", display.contentWidth*0.5, 2, GAME_FONT, 16 )
	expText:setReferencePoint(display.TopRightReferencePoint) 
	expText.text = RECORD_DATA[RECORD_EXP] .. " xp"
	expText:setTextColor(0,0,0,255)
	expText.x = 100
	expText.y = 40
	
    group:insert( expText )
    group:insert( coinText )
    
    -- display IAP buy coins
    
    -- display IAP buy all if locked
    
  
    if IS_KINDLE then
        print "kindle, no store"
    else
        if store.availableStores.google then
            store.init("google", transactionCallback)
        end
    end
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view

   coinText.text = RECORD_DATA[RECORD_GOLD] .. " coins"

     storyboard.removeScene( "level1" )
     
     
     if RECORD_DATA[RECORD_PURCHASED] == 0 and SHOWADS then
         
         local function adListener( event )
                if event.isError then
                    -- Failed to receive an ad.
                    print "AD FAILED"
                end
            end
            print "Display AD"
     	   ads.init( "inneractive", "SilentLogicStudios_DragonBurst_iPhone", adListener )
            ads.show( "banner", { x=10, y=0, interval=60 } )
     end
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	ads.hide()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene