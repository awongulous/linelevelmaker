local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local board = require( "board" )
local progressBar = require( "progressBar" )
local media = require( "media" )
local widget = require "widget"

local GAME_STARTTIME = 50
local gameBoard = nil
local timeBar = nil
local timerTime = 0
local gameover = false
local gameOverImage
local currentScore = 0
local currentGold = 0
local currentExp = 0
local TIMER_X = 100
local TIMER_Y = 40
local TIMER_WIDTH = 200
local TIMER_TEXTOFFSET = 20
local powerBar1
local powerBar2

matchSound1=media.newEventSound("Sounds/match1.wav")
matchSound2=media.newEventSound("Sounds/match2.wav")
explodeSound=media.newEventSound("Sounds/explode.wav")
function playSound(name)
  --  if RECORD_DATA[RECORD_SOUND] == 1 then
        media.playEventSound( name )
  --  end
end

local function onBackBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "menu", "fade", 500 )
	
	return true	-- indicates successful touch
end

local onComplete = function ( event )
	if "clicked" == event.action then
		local i = event.index
		if 2 == i then
			-- Player clicked 'Yes'; connect to facebook and submit score to server
 
			local loginListener = function( event )
				local shouldLoad = true
 
				local url = event.url
				if 1 == string.find( url, "corona:close" ) then
					-- Close the web popup
					shouldLoad = false
				end
 
				return shouldLoad
			end
 
			local oauthurl = "http://www.silentlogicstudios.com/match_app/facebookleaderboard/sendscore/?score=" .. currentScore
			native.showWebPopup( oauthurl, {urlRequest=loginListener} )
 
		elseif 1 == i then
			-- Player clicked 'No'; do nothing, just exit the dialog
		end
		
		if RECORD_DATA[RECORD_PURCHASED] == 0 and SHOWADS then

             local function adListener( event )
                    if event.isError then
                        -- Failed to receive an ad.
                        print "AD FAILED"
                    end
                end
                print "Display AD"
         	   ads.init( "inneractive", "SilentLogicStudios_DragonBurst_iPhone", adListener )
                ads.show( "fullscreen", { x=0, y=0, interval=60 } )
         end
	end
end

local function gameLoop()
    
    if gameover == false then
        
        if timeBar and timeBar:getCount() <= 0 then
            -- gameover
            gameover = true
            gameOverImage = display.newText("GAME OVER", 200, 100, GAME_FONT, 32)
            gameOverImage.x = 160
            gameOverImage.y = 240
            scene.view:insert(gameOverImage)
            
            local index = scList:CheckScore(currentScore)
        	print ("SCORE INDEX " .. index)
        	if index ~= 0 then
        	    scList:StoreEntry(index,currentScore,RECORD_DATA[RECORD_NAME])
        	    scList:Save(SCORE_FILE)
        	end
        	
        	if index == 1 then
        	    local alert = native.showAlert( "New High Score!", "Upload to Online Leaderboards?", 
                                                        { "YES", "NO" }, onComplete )
        	end
        	
        	RECORD_DATA[RECORD_EXP] = RECORD_DATA[RECORD_EXP] + currentExp
        	print ("Save exp " .. currentExp)
        	print ("Save gold " .. currentGold)
            RECORD_DATA[RECORD_GOLD] = RECORD_DATA[RECORD_GOLD] + currentGold
           	saveData()
           	
           	
            local function listener( event )
                gameover = false
                storyboard.gotoScene( "menu", "fade", 500 )
            end

            timer.performWithDelay(
               2000, listener )
            
            
            -- show summary, then show post prompt.
            -- 
            
        else
        
            if gameBoard then
                gameBoard:onUpdate()
            end
    
             if timeBar and timerTime < system.getTimer() then
                timeBar:setCount(timeBar:getCount()-1)
                local posx = timeBar:getFillScale()*TIMER_WIDTH + TIMER_TEXTOFFSET
                scene._timerText.x = posx
                local temp = "0:"
                if timeBar:getCount() < 10 then
                    temp = "0:0"
                end
                scene._timerText.text = temp .. ""..timeBar:getCount()
                timerTime = system.getTimer() + 1000
             end
        end
    else
        
        
    end
end


----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
-- 
---------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
 
-- Called when the scene's view does not exist:
function scene:createScene( event )
        local group = self.view
 
        -----------------------------------------------------------------------------
                
        --      CREATE display objects and add them to 'group' here.
        --      Example use-case: Restore 'group' from previously saved state.
        
        -----------------------------------------------------------------------------
        gameBoard = board.new(group, self)
        -- self._hud = display.newImageRect("Images/hud_bg.png", 320,100) 
        --        self._hud.x = 160
        --        self._hud.y = 50
        --        group:insert(self._hud)
        --        self._hud:toFront()
        
         --        self._distanceText = display.newText( "0 ft", display.contentWidth-30, 20, GAME_FONT, 16 )
        --         self._distanceText:setReferencePoint(display.TopRightReferencePoint) 
        -- group:insert(self._distanceText)
        -- 
        -- 
        -- timeBar = progressBar.new(group, TIMER_X, TIMER_Y, TIMER_WIDTH, 20, GAME_STARTTIME, self)
        -- timeBar:setCount(GAME_STARTTIME)
        -- 
        -- timerTime = system.getTimer() + 1000
        -- gameover = false
    	
        -- if HAS_BOMB then
        --          powerBar1 = progressBar.new(group, 100, 60, 100, 20, 100, self)
        --          powerBar1:setCount(0)
        --          powerBar1:setColor(0,0,255)
        --      end
        --      
        --      if HAS_FLARE then
        --          powerBar2 = progressBar.new(group, 100, 80, 100, 20, 100, self)
        --          powerBar2:setCount(0)
        --          powerBar2:setColor(255,0,0)
        --      end
        --      
        --      if HAS_STAR then
        --          powerBar3 = progressBar.new(group, 250, 60, 100, 20, 100, self)
        --          powerBar3:setCount(0)
        --          powerBar3:setColor(255, 10, 255)
        --      end
        --      
        --      if HAS_PLOW then
        --          powerBar4 = progressBar.new(group, 250, 80, 100, 20, 100, self)
        --          powerBar4:setCount(0)
        --          powerBar4:setColor(255, 255, 0)
        --      end
        --      
    	currentScore = 0
    	
    	-- self._scoreText = display.newText( "", display.contentWidth-30, 2, GAME_FONT, 16 )
    	--         self._scoreText:setReferencePoint(display.TopRightReferencePoint) 
    	--         self._scoreText.text = "0 pts"
    	--         group:insert(self._scoreText)
    	--         
    	--         self._coinText = display.newText( "", display.contentWidth*0.5, 2, GAME_FONT, 16 )
    	--         self._coinText:setReferencePoint(display.TopRightReferencePoint) 
    	--         self._coinText.text = "0 coins"
    	--         group:insert(self._coinText)
    	--         
    	--         local posx = timeBar:getFillScale()*TIMER_WIDTH + TIMER_TEXTOFFSET
    	--         self._timerText = display.newText( "", posx, TIMER_Y-15, GAME_FONT, 16 )
    	--         self._timerText:setReferencePoint(display.TopRightReferencePoint) 
    	--         self._timerText.text = "0:" .. timeBar:getCount()
    	
    	
        -- self._lineRect = display.newRect( 0, BOARD_STARTY+GEM_HEIGHT*LINE_ROW, display.contentWidth, 2 )
        --         group:insert(self._lineRect)
        --         
  --  	group:insert(self._timerText)
    	
    	local backBtn = widget.newButton{
    		label="Menu",
    		labelColor = { default={255}, over={128} },
    		defaultColor = {10,10,10},
    		overColor = {50,50,50},
    		strokeColor = {50,50,50},
    		default== "",
    		over="",
    		width=68, height=30,
    		onRelease = onBackBtnRelease	-- event listener function
    	}
    	backBtn:setReferencePoint( display.CenterReferencePoint )
    	backBtn.x = 32
    	backBtn.y = 15
    	
    	group:insert(backBtn)
      --  Runtime:addEventListener("enterFrame", gameLoop)
end

function scene:addScore(dt)
    currentScore = currentScore + dt
     self._scoreText.text = currentScore .. " pts"
end

function scene:addGold(dt)
   currentGold = currentGold + dt
   self._coinText.text = currentGold .. " coins"
end

function scene:addExp(dt)
   currentExp = currentExp + dt
  -- self._coinText.text = currentGold .. " coins"
end

function scene:addPower1(dt)
    if powerBar1 then
        powerBar1:setCount(powerBar1:getCount()+dt)
    
        if powerBar1:getCount() >= 100 then
            print "CREATE FLARE"
            powerBar1:setCount(0)
            gameBoard:createRandomBomb()
        end
    end
end

function scene:addPower2(dt)
    if powerBar2 then
        powerBar2:setCount(powerBar2:getCount()+dt)
    
        if powerBar2:getCount() >= 100 then
            print "CREATE FLARE"
            powerBar2:setCount(0)
            gameBoard:createRandomFlare()
        end
    end
    
end

function scene:addPower3(dt)
    if powerBar3 then
        powerBar3:setCount(powerBar3:getCount()+dt)
    
        if powerBar3:getCount() >= 100 then
            print "CREATE STAR"
            powerBar3:setCount(0)
            gameBoard:createRandomStar()
        end
    end
    
end

function scene:addPower4(dt)
    if powerBar4 then
        powerBar4:setCount(powerBar4:getCount()+dt)
    
        if powerBar4:getCount() >= 100 then
            print "CREATE PLOW"
            powerBar4:setCount(0)
            gameBoard:createRandomPlow()
        end
    end
    
end

function scene:increaseTime(dt)
    timeBar:setCount(timeBar:getCount()+dt)
end

function scene:updateDistance(dist)
    self._distanceText.text = dist .. " ft"
end
 
-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local group = self.view
        currentGold = 0
        currentExp = 0
        self._coinText.text = currentGold .. " coins"
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.782 or later.
        
        -----------------------------------------------------------------------------
        
end
 
-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local group = self.view
    
        -----------------------------------------------------------------------------
                
        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)
        
        -----------------------------------------------------------------------------
        
end
 
 
-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local group = self.view
        
        -----------------------------------------------------------------------------
        
        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
        
        -----------------------------------------------------------------------------
        Runtime:removeEventListener("enterFrame", gameLoop)
end
 
-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local group = self.view
        
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.782 or later.
        
        -----------------------------------------------------------------------------
        
end
 
 
-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
    local group = self.view
    Runtime:removeEventListener("enterFrame", gameLoop)
    if self._hud then
        self._hud:removeSelf()
        self._hud = nil
    end

    if self._distanceText then
        self._distanceText:removeSelf()
        self._distanceText = nil
    end

    if self._lineRect then
        self._lineRect:removeSelf()
        self._lineRect = nil
    end

    if self._distanceText then
        self._distanceText:removeSelf()
        self._distanceText = nil
    end

    if self._timerText then
        self._timerText:removeSelf()
        self._timerText = nil
    end

    if self._scoreText then
        self._scoreText:removeSelf()
        self._scoreText = nil
    end
    
    if self._coinText then
        self._coinText:removeSelf()
        self._coinText = nil
    end

    if gameBoard then
        gameBoard:destroy()
        gameBoard = nil
    end

    if timeBar then
        timeBar:destroy()
        timeBar = nil
    end

    if powerBar1 then
        powerBar1:destroy()
        powerBar1 = nil
    end

    if powerBar2 then
        powerBar2:destroy()
        powerBar2 = nil
    end

    if powerBar3 then
        powerBar3:destroy()
        powerBar3 = nil
    end
    
    if powerBar4 then
        powerBar4:destroy()
        powerBar4 = nil
    end
        -----------------------------------------------------------------------------
        
        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)
        
        -----------------------------------------------------------------------------
        
end
 
-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local group = self.view
        local overlay_scene = event.sceneName  -- overlay scene name
        
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.797 or later.
        
        -----------------------------------------------------------------------------
        
end
 
-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local group = self.view
        local overlay_scene = event.sceneName  -- overlay scene name
 
        -----------------------------------------------------------------------------
                
        --      This event requires build 2012.797 or later.
        
        -----------------------------------------------------------------------------
        
end
 
 
 
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
 
-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )
 
-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )
 
-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )
 
-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )
 
-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )
 
-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )
 
-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )
 
-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )
 
---------------------------------------------------------------------------------
 
return scene