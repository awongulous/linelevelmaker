-------------------------------------------------
--
-- board.lua
--

-------------------------------------------------

local board = {}
local board_mt = { __index = board }	-- metatable



local EMPTY_TYPE = -1

local TILE_TYPE1 = 1
local TILE_TYPE2 = 2
local TILE_TYPE3 = 3
local TILE_TYPE4 = 4
local TILE_TYPE5 = 5

local DIRT_TYPE1 = 10
local DIRT_TYPE2 = 11
local DIRT_TYPE3 = 12

local BOMB_TYPE1 = 13
local BOMB_TYPE2 = 14

local FLARE_TYPE1 = 15
local FLARE_TYPE2 = 16

local STAR_TYPE1 = 17
local STAR_TYPE2 = 18

local PLOW_TYPE1 = 19
local PLOW_TYPE2 = 20

local COIN_TYPE1 = 21
local COIN_TYPE2 = 22
local COIN_TYPE3 = 23

local hasFlare = false
local hasStar = false

local TILE_IMAGES = {   {TILE_TYPE1, "tile1.png", EMPTY_TYPE},
                        {TILE_TYPE2, "tile2.png", EMPTY_TYPE},
                        {TILE_TYPE3, "tile3.png", EMPTY_TYPE},
                        {TILE_TYPE4, "tile4.png", EMPTY_TYPE},
                        {TILE_TYPE5, "tile5.png", EMPTY_TYPE},
                        {BOMB_TYPE2, "bomb2.png", EMPTY_TYPE},
                        {FLARE_TYPE2, "flare2.png", EMPTY_TYPE},
                        {STAR_TYPE2, "star2.png", EMPTY_TYPE}
                        
}

local DIRT_IMAGES = {   {DIRT_TYPE1, "dirt1.png", EMPTY_TYPE},
                        {DIRT_TYPE2, "rock1.png", DIRT_TYPE1},
                        {DIRT_TYPE3, "rock2.png", DIRT_TYPE2},
                  --      {BOMB_TYPE1, "bomb1.png", BOMB_TYPE2},
                  --      {FLARE_TYPE1, "flare1.png", FLARE_TYPE2},
                 --       {STAR_TYPE1, "star1.png", STAR_TYPE2},
                        {COIN_TYPE1, "gem1.png", EMPTY_TYPE},
                        {COIN_TYPE2, "gem2.png", EMPTY_TYPE},
                        {COIN_TYPE3, "gem3.png", EMPTY_TYPE}

                    }
                    
local BOMB_COLOR = TILE_TYPE1
local FLARE_COLOR = TILE_TYPE2
local STAR_COLOR = TILE_TYPE3
local PLOW_COLOR = TILE_TYPE4

local arrows = { { }, { },{ },{ },{ },}
local map = { {"star1.png", 8,2, "star2.png", 2,4},
            {"bomb1.png", 6,3, "bomb2.png", 3,4},
            {"plow1.png", 2,6, "plow2.png", 5,3},
            {"flare1.png", 1,5, "flare2.png", 7,7},
            {"soundon.png", 5,7, "soundoff.png", 8,4}
}
-- coins for powerups.   points% give xp. level up unlocks powerup
local matchingIndex = 0
-------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------

local function getDogYears( realYears )	-- local; only visible in this module
	return realYears * 7
end

-------------------------------------------------
-- PUBLIC FUNCTIONS
-------------------------------------------------

function board.new( group , scene)	-- constructor

	local self = {
	--	name = name or "Unnamed",
	--	age = ageInYears or 2
        enabled = true,
        reconfiguring = false,
        currentFeet = 0
	}
	
    setmetatable( self, board_mt )
    self._owner = scene
    self._view = display.newGroup()
    self._tiles = {}
    self._removeList = {}
    self._currentType = EMPTY_TYPE
    self._updateSprites = {}
    for i=1,MAX_ROWS do
        self._tiles[i] = {}
        for j=1,MAX_COLS do
            local x = BOARD_STARTX + ((j-1)*GEM_WIDTH) + GEM_WIDTH*0.5
            local y = BOARD_STARTY + ((i-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
      
            local sprite

                sprite = display.newImageRect("Images/tile.png", GEM_WIDTH,GEM_HEIGHT)
    
            sprite.x = x
            sprite.y = y

            self.touch = onTouch
            sprite:addEventListener( "touch", self )
            self._view:insert(sprite)
            
 
            self._tiles[i][j] = {sprite, typeIndex, nil, nil, nil}
        end
        
    end    
    
    for i=1, #map do
        
        local name1 = map[i][1]
        local name2 = map[i][4]
        local col1 = map[i][2]
        local row1 = map[i][3]
        local col2 = map[i][5]
        local row2 = map[i][6]
        
        local x = BOARD_STARTX + ((col1-1)*GEM_WIDTH) + GEM_WIDTH*0.5
        local y = BOARD_STARTY + ((row1-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
  
        local sprite = display.newImageRect("Images/".. name1, GEM_WIDTH,GEM_HEIGHT)

        sprite.x = x
        sprite.y = y
        local tile = self._tiles[row1][col1] 
        tile[5] = sprite
        x = BOARD_STARTX + ((col2-1)*GEM_WIDTH) + GEM_WIDTH*0.5
        y = BOARD_STARTY + ((row2-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
        
        sprite = display.newImageRect("Images/".. name2, GEM_WIDTH,GEM_HEIGHT)

        sprite.x = x
        sprite.y = y
        
        tile = self._tiles[row2][col2] 
        tile[5] = sprite
    end
    
	group:insert(self._view)
	return self;
end

function board:destroy()
    for i=1,MAX_ROWS do
        for j=1,MAX_COLS do
            self._tiles[i][j][1]:removeEventListener( "touch", self )
            self._tiles[i][j][1]:removeSelf()
            self._tiles[i][j][1] = nil
        end
    end
    
    self._tiles = {}
end
-------------------------------------------------

function board:isDirt(type)
    for i=1, #DIRT_IMAGES do
       if DIRT_IMAGES[i][1] == type then
           return true
       end 
    end
    
    return false
end

function board:coinValue(type)

    print ("coin value " .. type)   
    if type == COIN_TYPE1 then
        return 10;
    elseif type == COIN_TYPE2 then
        return 20;
    elseif type == COIN_TYPE3 then
        return 30;
    end
    
    return 0
end

function board:createRandomPlow()
    
    local colors = {}
    for i=MAX_ROWS,1,-1 do
        for j=1,MAX_COLS do
            local tile = self._tiles[i][j]
            print ("TEST " .. tile[2] .. " " .. PLOW_COLOR)
            if self:isDirt(tile[2]) then
                print ("CHANGE")
                colors[#colors+1] = {i,j}
            end

        end
    end
    
    local r =  math.random( 1, #colors )
    local item = colors[r]
    local tile = self._tiles[item[1]][item[2]]
    local sprite = display.newImageRect("Images/plow2.png", GEM_WIDTH,GEM_HEIGHT) 
    sprite.x = tile[1].x
    sprite.y = tile[1].y

    self.touch = onTouch
    sprite:addEventListener( "touch", self )
    self._view:insert(sprite)

    self:clearTile(item[1],item[2], false)
    self._tiles[item[1]][item[2]] = {sprite, PLOW_TYPE2}
end


function board:createRandomBomb()
    
    local colors = {}
    for i=MAX_ROWS,1,-1 do
        for j=1,MAX_COLS do
            local tile = self._tiles[i][j]
            print ("TEST " .. tile[2] .. " " .. BOMB_COLOR)
            if tile[2] == BOMB_COLOR then
                print ("CHANGE")
                colors[#colors+1] = {i,j}
            end

        end
    end
    
    local r =  math.random( 1, #colors )
    local item = colors[r]
    local tile = self._tiles[item[1]][item[2]]
    local sprite = display.newImageRect("Images/bomb2.png", GEM_WIDTH,GEM_HEIGHT) 
    sprite.x = tile[1].x
    sprite.y = tile[1].y

    self.touch = onTouch
    sprite:addEventListener( "touch", self )
    self._view:insert(sprite)

    self:clearTile(item[1],item[2], false)
    self._tiles[item[1]][item[2]] = {sprite, BOMB_TYPE2}
end

function board:createRandomStar()
    
    local colors = {}
    for i=MAX_ROWS,1,-1 do
        for j=1,MAX_COLS do
            local tile = self._tiles[i][j]
            print ("TEST " .. tile[2] .. " " .. STAR_COLOR)
            if tile[2] == STAR_COLOR then
                print ("CHANGE")
                colors[#colors+1] = {i,j}
            end

        end
    end
    
    local r =  math.random( 1, #colors )
    local item = colors[r]
    local tile = self._tiles[item[1]][item[2]]
    local sprite = display.newImageRect("Images/star2.png", GEM_WIDTH,GEM_HEIGHT) 
    sprite.x = tile[1].x
    sprite.y = tile[1].y

    self.touch = onTouch
    sprite:addEventListener( "touch", self )
    self._view:insert(sprite)

    self:clearTile(item[1],item[2], false)
    self._tiles[item[1]][item[2]] = {sprite, STAR_TYPE2}
end


function board:createRandomFlare()
    
    local colors = {}
    for i=MAX_ROWS,1,-1 do
        for j=1,MAX_COLS do
            local tile = self._tiles[i][j]
            print ("TEST " .. tile[2] .. " " .. FLARE_COLOR)
            if tile[2] == FLARE_COLOR then
                print ("CHANGE")
                colors[#colors+1] = {i,j}
            end

        end
    end
    
    local r =  math.random( 1, #colors )
    local item = colors[r]
    local tile = self._tiles[item[1]][item[2]]
    local sprite = display.newImageRect("Images/flare2.png", GEM_WIDTH,GEM_HEIGHT) 
    sprite.x = tile[1].x
    sprite.y = tile[1].y

    self.touch = onTouch
    sprite:addEventListener( "touch", self )
    self._view:insert(sprite)

    self:clearTile(item[1],item[2], false)
    self._tiles[item[1]][item[2]] = {sprite, FLARE_TYPE2}
end

function board:isPowerup(type)

       if type == BOMB_TYPE2 or type == FLARE_TYPE2 or type == STAR_TYPE2 or type == PLOW_TYPE2 then
           return true
       end 
 
    
    return false
end

function board:getRandomDirt()

    -- dirt, powerup, gem
    local r =  math.random( 1, 9 )
    if r <= 7 then
       local dr =  math.random( 1, 10 )
       if dr <= 7 then        
           return DIRT_IMAGES[1]
       elseif dr == 9 then
           return DIRT_IMAGES[2]
       else
           return DIRT_IMAGES[3]
       end
    -- elseif r <= 8 then
    -- 
    --         return DIRT_IMAGES[4]
    else
        local gr =  math.random( 1, 100 )
        if gr < 50 then
            return DIRT_IMAGES[4]
        elseif gr < 90 then
            return DIRT_IMAGES[5]
        else
            return DIRT_IMAGES[6]
        end
    end
    
    return nil
end

function board:reconfigureTiles()
    for i=1,2 do
        for j=1,MAX_COLS do
            self._tiles[i][j][1]:removeEventListener( "touch", self )
            self._tiles[i][j][1]:removeSelf()
            self._tiles[i][j][1] = nil
        end
    end
    
    for i=1,MAX_ROWS do
        for j=1,MAX_COLS do
            if i <= MAX_ROWS then 
                local tile = self._tiles[i][j]
                local below = self._tiles[i+2][j]
                tile[1] = below[1]
                tile[2] = below[2]
            end
        end
    end
end

function board:onUpdate()
   
   
   local length = #self._updateSprites
   
   for i=length,1,-1 do 
       
       local sprite = self._updateSprites[i][1]
       if sprite then
           local desty = self._updateSprites[i][2]
     
           local posy = sprite.y;
       
           posy = posy+FALL_SPEED;
           if posy > desty then
       
               posy = desty;
               table.remove(self._updateSprites, i)
           end

           sprite.y = posy;
       else
           table.remove(self._updateSprites, i)
        end
        
   end 
   
   length = #self._updateSprites
   local removeLength = #self._removeList
   if (length == 0 and removeLength == 0 and self.enabled == false) then
       -- check if need to move up
       local needMove = true
       for i=LINE_ROW-2,LINE_ROW do
           for j=1,MAX_COLS do
               local tile = self._tiles[i][j]
               --if tile[2] == DIRT_TYPE1 or tile[2] == DIRT_TYPE2 or tile[2] == DIRT_TYPE3 then
               if self:isDirt(tile[2]) then
                   needMove = false
               end
           end
       end
       
       if needMove then
          self.reconfiguring = true
          needMove = false 
          
          print ("NEED MOVE") 
          -- add two rows of dirt at bottom
          for i=MAX_ROWS+1,MAX_ROWS+2 do
                self._tiles[i] = {}
                 for j=1,MAX_COLS do
                     local x = BOARD_STARTX + ((j-1)*GEM_WIDTH) + GEM_WIDTH*0.5
                     local y = BOARD_STARTY + ((i-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
                     
                     local dirt = self:getRandomDirt()
                     
                     local typeIndex = dirt[1]
                     sprite = display.newImageRect("Images/"..dirt[2], GEM_WIDTH,GEM_HEIGHT)
                     sprite.x = x
                     sprite.y = y
                     self.touch = onTouch
                     sprite:addEventListener( "touch", self )
                     self._view:insert(sprite)
                     self._tiles[i][j] = {sprite, typeIndex}
                     
                 end
          end
          self._liftCount = 0
          self._allLifting = true
          local listener1 = function( obj )
               --   print( "Transition 1 completed on object: " .. tostring( obj ) )
                  
                  self._liftCount = self._liftCount - 1
                  if self._liftCount == 0 and self._allLifting == false then
                     print "FInISHED LIFTING" 
                     self:reconfigureTiles()
                     self.reconfiguring = false
                  end
          end
          
          for i=1,MAX_ROWS+2 do
                 for j=1,MAX_COLS do
                     local tile = self._tiles[i][j]
                     self._liftCount = self._liftCount + 1
                     transition.to( tile[1], { time=500, y=tile[1].y-GEM_HEIGHT*2, onComplete=listener1 } )

                 end
          end
          self.currentFeet = self.currentFeet + 20
          if (self.currentFeet < 100) then
              self._owner:increaseTime(20)
          elseif (self.currentFeet < 200) then
              self._owner:increaseTime(10)
          else
              self._owner:increaseTime(10)
          end   
          
          self._owner:updateDistance(self.currentFeet)
          self._allLifting  = false   
       end
       self.enabled = true
   --    self:createRandomFlare()
   end
   
end

function board:addFallingTile(sprite, desty)
    local temp = {}
    temp[1] = sprite
    temp[2] = desty
    
    self._updateSprites[#self._updateSprites+1] = temp
end

function board:repairCol(index)

    local extension = 0
    for y=MAX_ROWS, 1, -1 do
   --     print ("TEST " .. y .. " " .. index)
        local tile = self._tiles[y][index]
        
        if tile[2] == EMPTY_TYPE then
            extension = extension + 1
        elseif extension == 0 then

        else
            local spot = extension+y;
            local tileSpot = self._tiles[spot][index]
            tileSpot[2] = tile[2]
            tile[2] = EMPTY_TYPE
            
            if tile[1] ~= nil then
                tileSpot[1] = tile[1]
                local spr = tile[1]
                if (spr) then
                    self:addFallingTile(spr, spr.y+GEM_HEIGHT*extension)
                end
            end
        end
    end
    
    local yoffset = 0;

    for i=extension, 1, -1 do
        local gemTypes = GEM_TYPES
        if self.currentFeet >= 100 and self.currentFeet < 200 then
            gemTypes = GEM_TYPES + 1
        elseif self.currentFeet >= 200 then
            gemTypes = GEM_TYPES + 2
        end
        
        local value = math.random( 1, gemTypes )
        local tileItem = self._tiles[i][index]
        tileItem[2] = value;

        local x = BOARD_STARTX + index*GEM_WIDTH-GEM_WIDTH*0.5;
        local y = -yoffset*GEM_HEIGHT-GEM_HEIGHT*0.5

        local sprite = display.newImageRect("Images/" .. TILE_IMAGES[value][2], GEM_WIDTH,GEM_HEIGHT) 
        sprite.x = x
        sprite.y = y
        
        local y2 = BOARD_STARTY + ((i)*GEM_HEIGHT)-GEM_HEIGHT*0.5;
        
        self.touch = onTouch
        sprite:addEventListener( "touch", self )
        self._view:insert(sprite)
        
        tileItem[1] = sprite

        self:addFallingTile(sprite, y2);

        yoffset = yoffset + 1;
    end
    
    return extension
end

function board:repair()
    local maxCount = 0;
    for x=1, MAX_COLS do
        local count = self:repairCol(x);
        if count > maxCount then
            maxCount = count;
        end
    end
    
    return maxCount;
end

function board:clearTile(r,c, gain)
    print ("REMOVE " .. r .. " - " .. c)
    local tile = self._tiles[r][c]
    
    if gain then
        
        if tile[2] == BOMB_COLOR then
           self._owner:addPower1(10)
        end
        if tile[2] == FLARE_COLOR then
           self._owner:addPower2(10)
        end
    
        if tile[2] == STAR_COLOR then
           self._owner:addPower3(10)
        end
    
        if tile[2] == PLOW_COLOR then
           self._owner:addPower4(10)
        end
    end
    
    local value = self:coinValue(tile[2]) 
    
    if value > 0 then
       self._owner:addGold(value) 
    end
    
    if tile[1] and tile[2] ~= EMPTY_TYPE then
        tile[1]:removeEventListener( "touch", self )
        tile[1]:removeSelf()
        tile[2] = EMPTY_TYPE
    end

end

function board:handleFlare(r, c)
    playSound(explodeSound)
    self._matchCount = self._matchCount + 1
    self._removeList[#self._removeList+1] = {r,c}
    
    for i=1,MAX_ROWS do
        local tile = self._tiles[i][c]
        if i ~= r and self:isPowerup(tile[2]) == false then
            self._matchCount = self._matchCount + 1
            self._removeList[#self._removeList+1] = {i,c}
        end
    end
    
    for j=1,MAX_COLS do
        local tile = self._tiles[r][j]
        if j ~= c and self:isPowerup(tile[2]) == false  then
            self._matchCount = self._matchCount + 1
            self._removeList[#self._removeList+1] = {r,j}
        end
    end
end

function board:handlePlow(r, c)
    playSound(explodeSound)
    self._matchCount = self._matchCount + 1
    if r == MAX_ROWS then
        r = MAX_ROWS -1
    end
    
    for i=r,r+1 do
        for j=1,MAX_COLS do
            local tile = self._tiles[i][j]
            if i <= MAX_ROWS and self:isPowerup(tile[2]) == false  then
                self._matchCount = self._matchCount + 1
                self._removeList[#self._removeList+1] = {i,j}
            end
        end
    end
end

function board:handleStar(r, c)
    playSound(explodeSound)
    self._matchCount = self._matchCount + 1
    self._removeList[#self._removeList+1] = {r,c}
    
    local startc = c-1
    local endc = c+1
    if startc < 1 then
        startc = 1
    end
    
    if endc > MAX_COLS then
        endc = MAX_COLS
    end
    
  
    local endr = r+3
    if endr >= MAX_ROWS then
        endr = MAX_ROWS
    end
    

    
    for i=startc,endc do
        for j=r,endr do
            local tile = self._tiles[j][i]
            if i >= 1 and i <= MAX_COLS and self:isPowerup(tile[2]) == false  then
                self._matchCount = self._matchCount + 1
                self._removeList[#self._removeList+1] = {j,i}
            end
        end
    end
end

function board:handleBomb(row, col)
    playSound(explodeSound)
    self._matchCount = self._matchCount + 1
    self._removeList[#self._removeList+1] = {row,col}

    if (row > 1 and col > 1) then
        local tile = self._tiles[row-1][col-1]
        if self:isPowerup(tile[2]) == false then
            self._matchCount = self._matchCount + 1
            self._removeList[#self._removeList+1] = {row-1,col-1}
        end
    end

    if (row > 1) then
        local tile = self._tiles[row-1][col]
        if self:isPowerup(tile[2]) == false then
            self._matchCount = self._matchCount + 1
	        self._removeList[#self._removeList+1] = {row-1,col}
	    end
    end

    if (row > 1 and col < MAX_COLS) then
        local tile = self._tiles[row-1][col+1]
        if self:isPowerup(tile[2]) == false then
	        self._matchCount = self._matchCount + 1
	        self._removeList[#self._removeList+1] = {row-1,col+1}
	    end
	end

    if col > 1 then
        local tile = self._tiles[row][col-1]
        if self:isPowerup(tile[2]) == false then
            self._matchCount = self._matchCount + 1
            self._removeList[#self._removeList+1] = {row,col-1}
        end
    end

    if col < MAX_COLS then
        local tile = self._tiles[row][col+1]
        if self:isPowerup(tile[2]) == false then
            self._matchCount = self._matchCount + 1
            self._removeList[#self._removeList+1] = {row,col+1}
        end
    end

    if (row < MAX_ROWS and col > 1) then
        local tile = self._tiles[row+1][col-1]
        if self:isPowerup(tile[2]) == false then
            self._matchCount = self._matchCount + 1
	        self._removeList[#self._removeList+1] = {row+1,col-1}
	    end
    end

    if (row < MAX_ROWS) then
        local tile = self._tiles[row+1][col]
        if self:isPowerup(tile[2]) == false then
	        self._matchCount = self._matchCount + 1
	        self._removeList[#self._removeList+1] = {row+1,col}
	    end
    end

    if col < MAX_COLS and row < MAX_ROWS then
        local tile = self._tiles[row+1][col+1]
        if self:isPowerup(tile[2]) == false then
	        self._matchCount = self._matchCount + 1
	        self._removeList[#self._removeList+1] = {row+1,col+1}
	    end
	end 
end

function board:checkMatches(row, col)
    
    local tile = self._tiles[row][col]
    if self:isDirt(tile[2]) then
        return
    end
    
    self._matchCount = 0    
    
    if self:isPowerup(tile[2]) then
        
        if tile[2] == BOMB_TYPE2 then
            -- for now assume bomb. Add surrounding tiles
            self:handleBomb(row, col)
            
        elseif tile[2] == FLARE_TYPE2 then
            -- flare
            self:handleFlare(row, col)
        elseif tile[2] == STAR_TYPE2 then
            -- star
            self:handleStar(row, col)
        elseif tile[2] == PLOW_TYPE2 then
            -- flare
            self:handlePlow(row, col)
        end
	    
	    for i=1,#self._removeList do
            local item = self._removeList[i]

            self:clearTile(item[1],item[2], false)
            self._owner:addScore(50)
            local tile2 = self._tiles[item[1]][item[2]]
            if self:isDirt(tile2[2]) then
                self._owner:addExp(50)
            end
        end
    else
        self:check(row, col)
    
        if self._matchCount < MATCHCOUNT_LOWEST then
            playSound(matchSound1)
        else
            playSound(matchSound2)
        end
            
        if self._matchCount >= MATCHCOUNT_LOWEST then
            for i=1,#self._removeList do
                local item = self._removeList[i]
             
                local tile2 = self._tiles[item[1]][item[2]]
                if tile2[2] >= DIRT_IMAGES[1][1] then
                    -- its dirt. check list 
                    if self._matchCount >= 3 then
                        local dirtInfo = self:getDirtInfoForType(tile2[2])
                        print ("NEXT TO DIRTs " .. dirtInfo[3])
                        if dirtInfo and dirtInfo[3] == EMPTY_TYPE then
                             print "CLEAR DIRTs"
                            self:clearTile(item[1],item[2], false)
                            self._owner:addScore(50)
                            self._owner:addExp(50)
                        else
                             print "CHANGE TO DIRTs"
                            -- change type to type
                            local info2 = self:getDirtInfoForType(dirtInfo[3])
                            local sprite = display.newImageRect("Images/" .. info2[2], GEM_WIDTH,GEM_HEIGHT) 
                            sprite.x = tile2[1].x
                            sprite.y = tile2[1].y
                    
                            self.touch = onTouch
                            sprite:addEventListener( "touch", self )
                            self._view:insert(sprite)
                    
                            self:clearTile(item[1],item[2], false)
                    
                            self._tiles[item[1]][item[2]] = {sprite, info2[1]}

                        end
                    end
                
                else
            
                    self:clearTile(item[1],item[2], true)
                    self._owner:addScore(50)
                end

             
            end
        end
    end
    
    print ("FINISH REMOVE")
    self._removeList = {}
    self._currentType = EMPTY_TYPE
    
    self:repair()
end

function board:getDirtInfoForType(type)
    for i=1, #DIRT_IMAGES do
        if DIRT_IMAGES[i][1] == type then
            return DIRT_IMAGES[i]
        end
    end
    
    for i=1, #TILE_IMAGES do
        if TILE_IMAGES[i][1] == type then
            return TILE_IMAGES[i]
        end
    end
    
    return nil
end

function board:check(row, col)
--	print( "CHECKING " .. row .. " : " .. col)
    
	if row < 1 or row > MAX_ROWS or col < 1 or col > MAX_COLS then
	    print "none"
	    return
	end
	local tile = self._tiles[row][col]

    if tile[2] == EMPTY_TYPE then
        print ("bad tile ".. row .. " " .. col)
        return
    end
    
	-- check dup
	for i=1,#self._removeList do
	    local item = self._removeList[i]
       -- if (self._tiles[row][col] == self._removeList[i]) then
        if row == item[1] and col == item[2] then
            return
        end        
    end
    
    if self:isDirt(tile[2]) then
        self._removeList[#self._removeList+1] = {row,col}
    end
    
	if self._currentType == EMPTY_TYPE or self._currentType == tile[2] then
	    self._matchCount = self._matchCount + 1
	    self._removeList[#self._removeList+1] = {row,col}
	    self._currentType = tile[2]
	    self:check(row+1, col)
	    self:check(row-1, col)
	    self:check(row, col+1)
	    self:check(row, col-1)
	end
--	self.checkLeft()
    
end

function board:addArrow(tile, type)
    
    local prevTile
    local starting = false
    if #arrows[type] == 0 then
        prevTile = tile
        starting = true
    else
        prevTile = arrows[type][#arrows[type]]
    end
    
    
    
    -- update forward arrow
    if starting then
        local sprite = display.newImageRect("Images/arrow_base.png", GEM_WIDTH,GEM_HEIGHT)
        sprite.x = tile[1].x
        sprite.y = tile[1].y
        sprite.arrowType = type;
        tile[3] = sprite;
        local count = #arrows[type]
        print ("count " .. count .. " " .. type)
        arrows[type][count+1] = tile
        
        if (type == 1) then
            sprite:setFillColor(255,0,0); 
            elseif (type == 2) then
                sprite:setFillColor(255,255,0); 
                elseif (type == 3) then
                    sprite:setFillColor(0,255,0);
                    elseif (type == 4) then
                        sprite:setFillColor(0,0,255);
                        elseif (type == 5) then
                            sprite:setFillColor(0,255,255);
                        end
    elseif starting == false then
        
        sprite = display.newImageRect("Images/arrow_line.png", GEM_WIDTH,GEM_HEIGHT)
        sprite.x = prevTile[1].x
        sprite.y = prevTile[1].y
        prevTile[4] = sprite;
        
        local forward = display.newImageRect("Images/arrow_line.png", GEM_WIDTH,GEM_HEIGHT)
        forward.x = tile[1].x
        forward.y = tile[1].y
        forward.arrowType = type
        tile[3] = forward;
        
        local diff2 = tile[1].x - prevTile[1].x
        local diff1 = tile[1].y - prevTile[1].y
        local rotation = 0
        local fromRotation = 0
        print ("DIFF1 " .. diff1)
        print ("DIFF2 " .. diff2)
        if diff1 == 0 and diff2 > 0 then
          -- right
            rotation = 0
            fromRotation = 180
        elseif diff1 == 0 and diff2 < 0 then
            -- left
            rotation = 180
            fromRotation = 0
        
        elseif diff1 < 0 and diff2 == 0 then
            -- top
            rotation = -90
            fromRotation = 90
        elseif  diff1 > 0 and diff2 == 0 then
            -- bottom
            rotation = 90
            fromRotation = -90
        elseif  diff1 < 0 and diff2 < 0 then
        -- UL
            rotation = -135
            fromRotation = 45
        elseif  diff1 < 0 and diff2 > 0 then
        -- UR
            rotation = -45
            fromRotation = 135
        elseif diff1 > 0 and diff2 < 0 then
         -- LL
            rotation = 135
            fromRotation = -45
        elseif diff1 > 0 and diff2 > 0 then
            -- LR
            rotation = 45
            fromRotation = -135
        end
        
        sprite.rotation = rotation
        sprite.arrowType = type
        forward.rotation = fromRotation
        local count = #arrows[type]
        arrows[type][count+1] = tile
        
        if (type == 1) then
            forward:setFillColor(255,0,0); 
            elseif (type == 2) then
                forward:setFillColor(255,255,0); 
                elseif (type == 3) then
                    forward:setFillColor(0,255,0);
                    elseif (type == 4) then
                        forward:setFillColor(0,0,255);
                        elseif (type == 5) then
                            forward:setFillColor(0,255,255);
                        end
                        
                        
                        if (type == 1) then
                            sprite:setFillColor(255,0,0); 
                            elseif (type == 2) then
                                sprite:setFillColor(255,255,0); 
                                elseif (type == 3) then
                                    sprite:setFillColor(0,255,0);
                                    elseif (type == 4) then
                                        sprite:setFillColor(0,0,255);
                                        elseif (type == 5) then
                                            sprite:setFillColor(0,255,255);
                                        end
    end
    
end

function board:clearArrows(type)
    for i=1,#arrows[type] do
        local tile = arrows[type][i]
        if tile[3] then
            tile[3]:removeSelf()
            tile[3] = nil
        end
        if tile[4] then
            tile[4]:removeSelf()
            tile[4] = nil 
        end
    end

    arrows[type] = {}
end
-------------------------------------------------
function onTouch( self, event )
    print "touch"
    
    if "began" == event.phase then
        for i=1,MAX_ROWS do
            for j=1,MAX_COLS do
                local tile = self._tiles[i][j]
                if tile[1] == event.target and tile[5] ~= nil then
                    for n=1,#map do

                        if (map[n][2] == j and map[n][3] == i ) or (map[n][5] == j and map[n][6] == i ) then
                            
                            if #arrows[n] > 0 then
                                self:clearArrows(n)
                            else
                                matchingIndex = n
                                self:addArrow(tile, matchingIndex)
                                print ("set mat " .. matchingIndex)
                            end
                        end     
                    end
                    
                end
            end
        end
    elseif "moved" == event.phase then
        for i=1,MAX_ROWS do
            for j=1,MAX_COLS do
                local tile = self._tiles[i][j]
                if tile[1] == event.target and matchingIndex ~= 0 and tile[3] == nil and tile[4] == nil then
                    
                    if  tile[5] == nil then
                        self:addArrow(tile, matchingIndex)
                    else    
                        for n=1,#map do

                            if (  ((map[n][2] == j and map[n][3] == i ) or (map[n][5] == j and map[n][6] == i))  and matchingIndex == n) then
            
                                self:addArrow(tile, matchingIndex)
                                print ("set mat " .. matchingIndex .. " " .. n)
                                
                            end 
                                
                        end
                        matchingIndex = 0
                    end
                elseif tile[1] == event.target and matchingIndex ~= 0 then
                    -- if its the same type, leave it, if its different remove
                    if matchingIndex ~= tile[3].arrowType then
                        self:clearArrows(tile[3].arrowType)
                    end
    
                    
                end
                -- local tile = self._tiles[i][j]
                --                 
                --                 if tile[1] == event.target and tile[5] ~= nil then
                --                     
                --                 end
              --  if (tile[1] == event.target and tile[3] == nil and tile[4] == nil and self.enabled and self.reconfiguring == false) then
                  --  self.enabled = false
                  --  self:checkMatches(i,j)
                  -- if tile[1] == event.target and (tile[5] ~= nil and tile[3] == nil and tile[4] == nil) then
                  --                       
                  --                       for n=1,#map do
                  --       
                  --                          if (map[n][2] == j and map[n][3] == i ) or (map[n][5] == j and map[n][6] == i ) then
                  --                              matchingIndex = n
                  --                              print ("set mat " .. matchingIndex)
                  --                          end     
                  --                       end
                  --                       self:addArrow(tile, matchingIndex)
                  --                     
                  --                       print ("FINISH TOUCH")
                  --                     return
                  --                   elseif (tile[1] == event.target and #arrows > 0 and tile[3] == nil and tile[4] == nil and self.enabled and self.reconfiguring == false) then
                  --                         local ok = false
                  --                       if tile[5] ~= nil then
                  --                           
                  --                           for n=1,#map do
                  --                              if (map[n][2] == j and map[n][3] == i ) or (map[n][5] == j and map[n][6] == i)  then
                  --                                  if matchingIndex == n then
                  --                                      ok = true
                  --                                  end
                  --                                  
                  --                              end     
                  --                           end
                  --                           
                  --                           if ok == false then
                  --                               return
                  --                           end
                  --                       end
                  --                       self:addArrow(tile, matchingIndex)
                  --                       
                  --                       if (ok) then
                  --                          print "MATCH MADE" 
                  --                       end
                  --                   end
                  --                     
            end
        end
    elseif "ended" == event.phase then    
    
        matchingIndex = 0
        -- for i=1,#arrows do
        --            local tile = arrows[i]
        --            if tile[3] then
        --                tile[3]:removeSelf()
        --                tile[3] = nil
        --            end
        --            if tile[4] then
        --                tile[4]:removeSelf()
        --                tile[4] = nil 
        --            end
        --         end
        --         
        --         arrows = {}
    end
end
-------------------------------------------------

return board