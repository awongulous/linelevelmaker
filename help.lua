-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"
local page
local group
--------------------------------------------

local background

-- 'onRelease' event listener for playBtn
local function onBackBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "menu", "fade", 500 )
	
	return true	-- indicates successful touch
end

local function onNextBtnRelease()
    
    page = page + 1
    if page > 5 then
        page = 1
    end    
        
    background:removeSelf()
    background = display.newImageRect( IMAGE_PATH .. "gravhelp" .. page .. ".png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	group:insert( background )
	background:toBack()	 
end

local function onPrevBtnRelease()
    
    page = page - 1
    if page < 1 then
        page = 5
    end    
        
    background:removeSelf()
    background = display.newImageRect( IMAGE_PATH .. "gravhelp" .. page .. ".png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	group:insert( background )
	background:toBack()	 
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	group = self.view
    page = 1
	-- display a background image
    background = display.newImageRect( IMAGE_PATH .. "gravhelp" .. page .. ".png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	group:insert( background )
	background:toBack()
	
	-- create a widget button (which will loads level1.lua on release)
	backBtn = widget.newButton{
		label="Menu",
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		default== "",
		over="",
		width=68, height=30,
		onRelease = onBackBtnRelease	-- event listener function
	}
	backBtn:setReferencePoint( display.CenterReferencePoint )
	backBtn.x = display.contentWidth*0.5
	backBtn.y = display.contentHeight-25
	
	
	nextBtn = widget.newButton{
	    label = "Next",
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		default== "",
		over="",
		width=68, height=30,
		onRelease = onNextBtnRelease	-- event listener function
	}
	nextBtn:setReferencePoint( display.CenterReferencePoint )
	nextBtn.x = display.contentWidth-40
	nextBtn.y = display.contentHeight-25
	
	prevBtn = widget.newButton{
	    label = "Prev",
		labelColor = { default={255}, over={128} },
		defaultColor = {10,10,10},
		overColor = {50,50,50},
		strokeColor = {50,50,50},
		default== "",
		over="",
		width=68, height=30,
		onRelease = onPrevBtnRelease	-- event listener function
	}
	prevBtn:setReferencePoint( display.CenterReferencePoint )
	prevBtn.x = 40
	prevBtn.y = display.contentHeight-25
	

	-- all display objects must be inserted into group
	group:insert( backBtn )	-- you must insert .view property for widgets
	group:insert( prevBtn )
	group:insert( nextBtn )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene