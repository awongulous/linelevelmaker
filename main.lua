require("pprint")
local pathfinder = require("pathfinder")
GEM_WIDTH = 40
GEM_HEIGHT = 40

level = 30
MARKS_MAX = 9
MAX_COLS = 9
MAX_ROWS = 9

BOARD_STARTX = 0
BOARD_STARTY = 120

marks = {}

map = {}
for i=1,MAX_COLS do
    map[i-1] = {}
    for j=1,MAX_ROWS do
        local x = BOARD_STARTX + ((i-1)*GEM_WIDTH) + GEM_WIDTH*0.5
        local y = BOARD_STARTY + ((j-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
  
        local sprite

        sprite = display.newImageRect("Images/tile.png", GEM_WIDTH,GEM_HEIGHT)
        sprite.x = x
        sprite.y = y
        map[i-1][j-1] = 1
    end
    
end    

function setRectColor(rect, k, alpha)
    local num = 255
    
    if alpha then
        num = 155
    end
    
    if (k == 1) then
        rect:setFillColor(num, 0, 0)
    elseif k == 2 then
        rect:setFillColor(0, 0, num)
    elseif k == 3 then
        rect:setFillColor(0, num, 0)
    elseif k == 4 then
        rect:setFillColor(num, num, 0)
    elseif k == 5 then
        rect:setFillColor(num/2, 0, 0)
    elseif k == 6 then
        rect:setFillColor(0, 0, num/2)
    elseif k == 7 then
        rect:setFillColor(num/2, 0, num/2)
    elseif k == 8 then
        rect:setFillColor(0, num/2, 0)
    else
        rect:setFillColor(0, 0, 0)
    end
    
end

function isAdj(c1,r1, c2,r2)
    if (c1 == nil or r1 == nil) then
        return false
  end
  
   local diff1 = math.abs(c1-c2)
   local diff2 = math.abs(r1-r2)
  
  --print ("test1 " .. c1 .. " " .. r1)
  --print ("test2 " .. c2 .. " " .. r2)
   if (diff1 == 0 and diff2 <= 1) then
      return true 
   end 
   
   if (diff2 == 0 and diff1 <= 1) then
         return true 
      end
      
    return false
end

function findSpot(c1,r1)
    
    local found = false
    local tries = 0
    while (tries < 100) do
        print ("look " .. tries)
        local srow =  math.random( 1, MAX_ROWS )
        local scol =  math.random( 1, MAX_COLS )
        
        if map[scol-1][srow-1] == 1 and isAdj(c1,r1,scol,srow) == false then
            print ("found " .. scol .. " " .. srow)
           return {scol, srow} 
        end
        tries = tries + 1
    end
    
    print ("CAN't FIND")
    
    return nil
end

k = 1
blocktries = 0
while (k <= MARKS_MAX) do
    print ("ON K " .. k)
    local spot = findSpot()
    if (spot == nil) then
        return
    end
    local scol =  spot[1]
    local srow =  spot[2]
map[scol-1][srow-1] = 0
        
   
                local spot = findSpot(scol, srow)
                if (spot == nil) then
                    return
                end
                local ecol =  spot[1]
                local erow =  spot[2]
map[scol-1][srow-1] = 1


    
    
        print ("start:  " .. scol .. " " .. srow)
        print ("end:  " .. ecol .. " " .. erow)
        local path = pathfinder.pathFind(map, MAX_COLS, MAX_ROWS, scol-1, srow-1, ecol-1, erow-1)
        pprint("Path", path)
        if (path == false) then
            print "###### Blocked"
            blocktries = blocktries + 1
            if blocktries >= 10 then
                print "###### EXIT EARLY"
                return
            end
        else
            blocktries = 0
            marks[k] = { scol,srow , ecol,erow}
            
            local myRectangle = display.newCircle(0, 0, 15)
                  myRectangle.strokeWidth = 3

                  setRectColor(myRectangle, k, false)
                  myRectangle:setStrokeColor(180, 180, 180)
                  myRectangle.alpha = 0.5

                  local x = BOARD_STARTX + ((scol-1)*GEM_WIDTH) + GEM_WIDTH*0.5
                  local y = BOARD_STARTY + ((srow-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
                  myRectangle.x = x
                  myRectangle.y = y
                          local myRectangle = display.newCircle(0,0,15)
                                myRectangle.strokeWidth = 3
                                setRectColor(myRectangle, k, false)
                                myRectangle:setStrokeColor(180, 180, 180)
                                myRectangle.alpha = 0.5

                                local x = BOARD_STARTX + ((ecol-1)*GEM_WIDTH) + GEM_WIDTH*0.5
                                local y = BOARD_STARTY + ((erow-1)*GEM_HEIGHT) + GEM_HEIGHT*0.5
                                myRectangle.x = x
                                myRectangle.y = y
        
        
            map[scol-1][srow-1] = 0
            map[ecol-1][erow-1] = 0
            local px = scol-1
            local py = srow-1
            for i=0, #path do
        
                local p = path[i]
        
                for n=1, p.count do
                    px = px + p.dx
                    py = py + p.dy
                    map[px][py] = 0

                      local myRectangle = display.newRect(0, 0, 30, 30)
                            myRectangle.strokeWidth = 3
                            setRectColor(myRectangle, k, false)
                            myRectangle:setStrokeColor(180, 180, 180)
                            myRectangle.alpha = 0.5

                            local x = BOARD_STARTX + ((px)*GEM_WIDTH) + GEM_WIDTH*0.5
                            local y = BOARD_STARTY + ((py)*GEM_HEIGHT) + GEM_HEIGHT*0.5
                            myRectangle.x = x
                            myRectangle.y = y
                 end
            end
            k = k+1
           -- pprint("Map", map)
        end
end
   
   
-- for i=1, #marks do
--    print (marks[i][1] .. "," .. marks[i][2] .. "," .. marks[i][3] .. "," .. marks[i][4])
-- end   
--    

local path = system.pathForFile( "level" .. level .. "_" .. MAX_COLS .. "x" .. MAX_ROWS .. ".txt", system.DocumentsDirectory )

local function writeLevel()
    local file = io.open(path, "wb")
    file:write(MARKS_MAX)
    file:write("\n")
    for i=1, #marks do
        file:write(marks[i][1] .. "," .. marks[i][2] .. "," .. marks[i][3] .. "," .. marks[i][4])
        file:write("\n")
    end
    io.close(file)
end
writeLevel()


-- local path = system.pathForFile( "level.txt", system.ResourceDirectory )
-- 
-- local function readLevel()
--     local file = io.open(path, "r")
--     for lines in file:lines() do
--         print (lines)
--     end
--     io.close(file)
-- end
-- readLevel()
