local progressBar = {}
local progressBar_mt = { __index = progressBar }	-- metatable

function progressBar.new( group , x,y,w,h,total, scene)	-- constructor

	local self = {

	}
	
    setmetatable( self, progressBar_mt )
    self._owner = scene
    self._view = display.newGroup()
	group:insert(self._view)
	
	self._total = total
	self._count = 0
	self._view.x = x
	self._view.y = y
	
	self._progressBG = display.newImageRect("Images/progressbar_bg.png", w,h)
	self._progressBG:setReferencePoint(display.TopLeftReferencePoint)
	self._progressFill = display.newImageRect("Images/progressbar_fill.png", w,h)  
    self._progressFill:setReferencePoint(display.TopLeftReferencePoint)
	self._progressFill.xScale = self._count/self._total
	self._view:insert(self._progressBG)
	self._view:insert(self._progressFill)
	return self
end

function progressBar:destroy()
    if self._progressBG then
        self._progressBG:removeSelf()
        self._progressBG = nil
    end

    if self._progressFill then
        self._progressFill:removeSelf()
        self._progressFill = nil
    end
   
end

function progressBar:setColor(r,g,b)
    self._progressBG:setFillColor(r,g,b)
    self._progressFill:setFillColor(r,g,b)
end

function progressBar:setCount(count)
    self._count = count
    if (self._count <= 0) then
        self._count = 0
    end
    
    if (self._count > self._total) then
        self._count = self._total
    end
  --  self._progressFill.x=0
   -- self._progressFill.xScale = self._count/self._total
   transition.to(self._progressFill, {time=500, xScale=self._count/self._total, transition = easing.linear})
end

function progressBar:getCount()
   return self._count 
end

function progressBar:getFillScale()
   return self._progressFill.xScale
end

return progressBar